import { Container, Row, Col, Card } from 'react-bootstrap';

export default function Highlights () {
		  const highlightsData = [
		    {
		      title: 'Convenience',
		      description: 'Our online shop is easy to use',
		    },
		    {
		      title: 'Quality',
		      description: 'Rest assured, all our products are original and not fakes',
		    },
		    {
		      title: 'Variety',
		      description: 'Discover a wide brands of vaping needs with juices and with pods.',
		    },
		    {
		      title: 'Promotions & Discounts',
		      description: 'Take advantage of our regular promotions and discounts, saving you money on your favorite vaping products.',
		    },
		  ];

	return(
		<section className="highLights">
		      <Container>
		        <h2 className="text-center mb-4">Why Choose Us</h2>
		        <Row>
		          {highlightsData.map((highlight, index) => (
		            <Col key={index} md={6} xs={12}>
		              <Card className="highLight">
		                <Card.Body>
		                  <Card.Title className="highlight__title">{highlight.title}</Card.Title>
		                  <Card.Text className="highlight__description">{highlight.description}</Card.Text>
		                </Card.Body>
		              </Card>
		            </Col>
		          ))}
		        </Row>
		      </Container>
		    </section>
	)
}