import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import {NavLink, Link} from 'react-router-dom'
import {useState, useEffect, useContext} from  'react'

import UserContext from '../UserContext'

export default function AppNavBar() {

   const {user} = useContext(UserContext);
  
   return(
      <Navbar bg="primary" expand="lg">
             <Container fluid>
                <Navbar.Brand as = {Link} to = '/'>Kenny VapeShop</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                    <Nav.Link as = {NavLink} to = '/' >Home</Nav.Link>
                    <Nav.Link as = {NavLink} to = '/products' >Products</Nav.Link>
                    
                    {
                        user.id === null || user.id === undefined
                        ?
                         <>
                           <Nav.Link as = {NavLink} to = '/register' >Register</Nav.Link>
                           <Nav.Link as = {NavLink} to = '/login' >Login</Nav.Link>
                         </>
                        :
                         (
                            <>
                                {user.isAdmin && (
                                  <Nav.Link className="page-header1" as={NavLink} to="/admin" style={{color: '#FDFDFB', marginTop: '5px'}}>
                                    Admin Panel
                                  </Nav.Link>
                                )}
                                <Nav.Link className="page-header1" as={NavLink} to="/logout" style={{color: '#FDFDFB', marginTop: '5px'}}>
                                  Logout
                                </Nav.Link>
                            </>
                    )
                    }

                   </Nav>
               </Navbar.Collapse>
            </Container>
       </Navbar>
   )
}
