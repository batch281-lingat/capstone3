import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

import {Link} from 'react-router-dom'

export default function ProductCard({productProp}) {
  // Checks to see if the data was successfully pased
  // console.log(courseProp);
  
 const { _id, name, description, price, image } = productProp;

 return (
    <Card className="d-flex" style={{width: '18rem'}} >
      <Card.Img variant="top" src={image} alt={name} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{`₱ ${price}`}</Card.Text>
        <Button as={Link} to={`/products/${_id}`} variant="primary">
          Details
        </Button>
      </Card.Body>
    </Card>
  )
}
ProductCard.propTypes = {
  productProp: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};

