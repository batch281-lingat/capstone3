import { Fragment, useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';

export default function Products(){
	

	const [products, setProducts] = useState([]);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(null);

	useEffect(() => {
		const fetchProducts = async () => {
			try {
				const response = await fetch(`${process.env.REACT_APP_API_URL}/products/active`);
				const data = await response.json();
				setProducts(data.map(product => {
					return(
						<ProductCard key = {product._id} productProp = {product} />
						)
				}));
				setLoading(false);
			} catch (err) {
				setError(err.message);
				setLoading(false);
			}
		};
		fetchProducts();
	}, []);

	return (
		<Fragment className = "products">
			{loading && <p>Loading...</p>}
			{error && <p>{error}</p>}
			{products}
		</Fragment>
	)
}
