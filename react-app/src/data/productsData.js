const productsData = [
   {
     id: 1,
     name: 'Oxva Xlim Pro',
     description: 'Is a sleek and user-friendly pod system designed to provide a satisfying vaping experience. With its slim and compact design, it easily fits in your pocket or purse, making it perfect for on-the-go vaping.',
     price: 1200,
     image: ''
   },
   {
     id: 2,
     name: 'OXVA Xlim Pro Cartridge(Replacement)',
     description: 'Is a high-quality replacement pod designed specifically for use with the OXVA Xlim Pro pod system. This cartridge offers a seamless vaping experience with its convenient design and exceptional performance.',
     price: 300,
     image: ''
   },
   {
     id: 3,
     name: 'OXVA Lanyard',
     description: 'Lanyard for your pod system',
     price: 150,
     image: ''
   }
 ]

 export default productsData